package xyz.elarvee.pdm

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter
import xyz.elarvee.pdm.jpa.entity.Entry
import xyz.elarvee.pdm.jpa.entity.Tag

@Suppress("FinalClassOrFunSpring")
@SpringBootApplication
@EntityScan(basePackageClasses = arrayOf(Application::class, Jsr310JpaConverters::class))
class Application : RepositoryRestConfigurerAdapter() {

    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration) {
        config.exposeIdsFor(Entry::class.java, Tag::class.java)
    }

}

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}
