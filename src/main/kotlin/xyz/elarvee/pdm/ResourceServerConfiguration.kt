package xyz.elarvee.pdm

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod.*
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@Suppress("FinalClassOrFunSpring")
@Configuration
@EnableResourceServer
class ResourceServerConfiguration : ResourceServerConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http.antMatcher("/**").authorizeRequests().anyRequest().authenticated().and().cors()
    }

    @Bean
    fun corsConfigurationSource() = UrlBasedCorsConfigurationSource().apply {
        this.registerCorsConfiguration("/**", CorsConfiguration().apply {
            this.allowedOrigins = listOf("*")
            this.allowedHeaders = listOf("*")
            this.allowedMethods = listOf(GET.name, POST.name, PUT.name, PATCH.name, DELETE.name)
            this.allowCredentials = true
        })
    }

}
