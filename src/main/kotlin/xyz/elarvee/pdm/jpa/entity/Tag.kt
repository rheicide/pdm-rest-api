package xyz.elarvee.pdm.jpa.entity

import javax.persistence.*

@Suppress("unused", "UNUSED_PARAMETER")
@Entity
class Tag(@Column(unique = true)
          val name: String,

          @ManyToMany(mappedBy = "tags")
          val entries: Set<Entry> = emptySet(),

          @Id @GeneratedValue
          val id: Long = -1) {

    constructor(reference: String) : this(name = "")

}
