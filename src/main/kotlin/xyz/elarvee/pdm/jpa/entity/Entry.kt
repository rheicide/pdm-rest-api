package xyz.elarvee.pdm.jpa.entity

import java.time.LocalDate
import javax.persistence.*

@Entity
class Entry(val note: String,
            val date: LocalDate,

            @ManyToMany
            @JoinTable(name = "entry_tag",
                    joinColumns = arrayOf(JoinColumn(name = "entry_id")),
                    inverseJoinColumns = arrayOf(JoinColumn(name = "tag_id")))
            val tags: Set<Tag> = emptySet(),

            @Id @GeneratedValue
            val id: Long = -1)
