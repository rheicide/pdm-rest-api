package xyz.elarvee.pdm.jpa.projection

import org.springframework.data.rest.core.config.Projection
import xyz.elarvee.pdm.jpa.entity.Entry
import xyz.elarvee.pdm.jpa.entity.Tag
import java.time.LocalDate

@Projection(name = "inlineTags", types = arrayOf(Entry::class))
interface InlineTags {

    fun getId(): Long

    fun getNote(): String

    fun getDate(): LocalDate

    fun getTags(): Set<Tag>

}
