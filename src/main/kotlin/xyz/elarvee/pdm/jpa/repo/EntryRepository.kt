package xyz.elarvee.pdm.jpa.repo

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.format.annotation.DateTimeFormat.ISO
import xyz.elarvee.pdm.jpa.entity.Entry
import xyz.elarvee.pdm.jpa.projection.InlineTags
import java.time.LocalDate

@Suppress("unused")
@RepositoryRestResource(excerptProjection = InlineTags::class)
interface EntryRepository : PagingAndSortingRepository<Entry, Long> {

    fun findByNoteContainingIgnoreCase(@Param("q") note: String, pageable: Pageable): Page<Entry>

    fun findByDate(@Param("q") @DateTimeFormat(iso = ISO.DATE) date: LocalDate, pageable: Pageable): Page<Entry>

    fun findByTagsName(@Param("q") tag: String, pageable: Pageable): Page<Entry>

    fun findByTagsNameIn(@Param("q") tags: Set<String>, pageable: Pageable): Page<Entry>

}
