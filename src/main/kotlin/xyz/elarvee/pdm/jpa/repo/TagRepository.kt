package xyz.elarvee.pdm.jpa.repo

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import xyz.elarvee.pdm.jpa.entity.Tag

@Suppress("unused")
@RepositoryRestResource
interface TagRepository : PagingAndSortingRepository<Tag, Long> {

    fun findByNameContainingIgnoreCase(@Param("q") name: String, pageable: Pageable): Page<Tag>

    fun findByName(@Param("q") name: String): Iterable<Tag>

}
