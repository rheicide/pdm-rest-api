package xyz.elarvee.pdm

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit4.SpringRunner

@Suppress("SpringKotlinAutowiredMembers")
@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class E2ETests {

    @Autowired
    lateinit var restTemplate: TestRestTemplate

    @Test
    fun should_get_entries() {
        val body = this.restTemplate.getForObject("/entries", Map::class.java)
        println(body)
        assertThat(body).isNotEmpty
    }

}
