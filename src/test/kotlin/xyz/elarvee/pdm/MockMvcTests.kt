package xyz.elarvee.pdm

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@Suppress("SpringKotlinAutowiredMembers")
@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
class MockMvcTests {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    fun should_get_entries() {
        this.mockMvc.perform(get("/entries"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$._embedded.entries").isNotEmpty)
    }

}
